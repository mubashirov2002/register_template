# DATABASE COMMAND 
  - sudo -u postgres psql

# Set up database settings
  - CREATE USER register_user WITH PASSWORD 'register_root'; 
  - CREATE DATABASE register_db OWNER register_user;


# Run command for migrate all migrations
  - python manage.py migrate


# Run command for create superuser
  - python manage.py createsuperuser
  - username : register_admin
  - password : 123456


# prod user
  - username : register_admin
  - password : 123456
