from django.shortcuts import render, redirect
from apps.user.models import User, Sms
from apps.user.forms import RegisterForm
from django.contrib.auth import authenticate, login



def register(request):
    form = RegisterForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            response = form.save()
            template = 'code_validate.html'
            response = render(request, template, {'code': response})
            response.set_cookie("fullname", request.POST.get('fullname'))
            response.set_cookie("birthday", request.POST.get('birthday'))
            response.set_cookie("phone_number", request.POST.get('phone_number'))
            response.set_cookie("email", request.POST.get('email'))
            response.set_cookie("password", request.POST.get('password'))
            return response
    ctx = {
        'form': form
    }
    return render(request, 'index.html', ctx)


def validate_code(request):
    phone = request.COOKIES.get('phone_number')
    email = request.COOKIES.get('email')
    if request.POST:
        if email and email != '':
            sms = Sms.objects.filter(email=email).first()
        else:
            sms = Sms.objects.filter(phone_number=phone).first()
        if sms:
            if sms.code == request.POST.get('code'):
                user = User(
                    username = phone if phone else email,
                    fullname = request.COOKIES.get('fullname'),
                    birthday= request.COOKIES.get('birthday'),
                    phone_number = phone,
                    email = email,
                )
                user.save()
                user.set_password(request.COOKIES.get('password'))
                user.save()
                login(request, user)
                return redirect('user:succes-page')
            else:
                message = "Tasdiqlash kodi xato qaytadan kiriting"
                ctx = {
                    "message":message,
                    "code": sms.code
                }
                return render(request, 'code_validate.html', ctx)

    
    return redirect('user:register')

def succes_page(request):
    return render(request, 'succes.html')