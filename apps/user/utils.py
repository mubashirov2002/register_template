import random
import string
from contextlib import closing
from django.db import connection


SHORTCODE_MIN = 8

def code_generator(size=SHORTCODE_MIN, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))