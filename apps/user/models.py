from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import UserManager, PermissionsMixin
from django.utils import timezone
from datetime import timedelta


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user for control"""

    username = models.CharField("Пользователь", max_length=25, unique=True)
    email = models.EmailField("Email", null=True, blank=True)
    fullname = models.CharField("Полное имя", max_length=150)
    birthday = models.DateField(null=True, blank=True)
    phone_number = models.CharField(
        "Номер телефона", max_length=15, null=True, blank=True
    )

    date_joined = models.DateTimeField("Дата регистрации", auto_now_add=True)
    is_active = models.BooleanField("is_active", default=True)
    is_staff = models.BooleanField("is_staff", default=False)
    user_photo = models.ImageField(upload_to="users_photos/", null=True, blank=True)

    objects = UserManager()

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.fullname

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"
        ordering = ['-date_joined']


class SmsManager(models.Manager):

    def create_sms(self, phone_number, email, code, type):
        creation_args = {
            'phone_number': phone_number,
            'email': email,
            'code': code,
            'sms_type': type,
            'expires': timezone.now() + timedelta(seconds=600)
        }
        sms = self.create(**creation_args)
        return sms


class Sms(models.Model):
    sms_type = models.PositiveSmallIntegerField(null=False, blank=False)
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    code = models.CharField(max_length=20, )
    is_active = models.BooleanField(default=False, null=False, blank=True)
    expires = models.DateTimeField()
    created_datetime = models.DateTimeField(auto_now=False, auto_now_add=True, null=True, editable=False)
    objects = SmsManager()

    class Meta:
        verbose_name = "User sms"

    def __str__(self):
        return self.phone_number or self.email
