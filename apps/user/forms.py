import re
from django import forms
from django.core.validators import RegexValidator
from datetime import datetime
from apps.user.models import User, Sms
from django.core.mail import send_mail, BadHeaderError
from .utils import code_generator
import threading
import requests
import json


class RegisterForm(forms.Form):
    fullname = forms.CharField(
        label="Ism Familiya:", max_length=100, required=True,
        error_messages={'required': 'Ism Familiyangizni kiriting!'},
        widget=forms.TextInput(attrs={'class': 'input--style-4'})
    )
    birthday = forms.DateField(
        label="Tug'ilgan kun:", required=True,
        error_messages={'required': "Tug'ilgan kuningizni kiriting"},
        widget=forms.DateInput(format="%d/%m/%Y", attrs={"class": "input--style-4", 'type': 'date'})
    )

    password = forms.CharField(
        label="Parol:", required=True, min_length=8,
        error_messages={
            'required': "Parol kiritishingiz shart",
            "length": "Kamida 8 ta belgidan iborat bo'lishi kerak",
            "invalid": "Kamida 8 ta belgidan iborat kod(Kotta, kichik harflardan, belgi va raqamdan foydalaning)"
        },
        widget=forms.PasswordInput(attrs={'class': 'input--style-4'})
    )
    phone_number = forms.CharField(
        label="Tel:", max_length=15, required=False,
        error_messages={
            'required': "Telefon raqamingizni kiritishiz shart",
            'invalid': "Telefon nomeringiz o'zbekcha uzmobile nomer bo'lishi kerak"
        },
        widget=forms.TextInput(attrs={'class': 'input--style-4', 'value': '+998', 'required': True})
    )
    email = forms.EmailField(
        label="Email:", required=False,
        widget=forms.EmailInput(attrs={'class': 'input--style-4', 'required': False})
    )

    def clean_birthday(self):
        cd = self.cleaned_data
        today = datetime.today()
        if (today.year - cd['birthday'].year) <= 18:
            raise forms.ValidationError('Yoshingiz 18 yoshdan katta bo`lishi kerak')
        return cd['birthday']

    def clean_password(self):
        password = self.cleaned_data['password']
        pattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$"
        validate_password = re.match(pattern, password)
        if not validate_password:
            raise forms.ValidationError("Parolda katta harf va son qatnashishi kerak")
        return password
    
    def clean_phone_number(self):
        cd = self.cleaned_data
        user = User.objects.filter(phone_number=cd['phone_number']).first()
        if len(cd['phone_number']) == 13:
            if user:
                raise forms.ValidationError("Bu telefon nomer ro'yxatdan o'tgan!")
            elif '+9989' != cd['phone_number'][:5]:
                raise forms.ValidationError("Telefon nomeringiz +9989 dan boshlanishi kerak!")
        return cd['phone_number']
    
    def createSmsObject(self, sms_code, phone=None, email=None):
        try: 
            if phone:
                sms = Sms.objects.get(phone_number=phone)
            else:
                sms = Sms.objects.get(email=email)
            sms.code = sms_code
            sms.expires = timezone.now() + timedelta(seconds=600)
            sms.save()
            return sms
        except Exception as e:
            sms = Sms.objects.create_sms(phone_number=phone, email=email, code=sms_code, type=1)
            if sms:
                return sms
            return False

    def save(self):
        data = self.cleaned_data
        sms_code = code_generator(size=6)
        if data.get('email') and data.get('email') != "":
            sms = self.createSmsObject(sms_code, email=data.get('email'))
            try:
                send_mail(
                'Code',
                f"{sms_code} - Tasdiqlash kodi",
                data.get('email'),
                ['testregister00@mail.ru'],
                fail_silently=False,)
            except Exception as e:
                print('send email error', e)
        else:
            sms = self.createSmsObject(sms_code, phone=data.get('phone_number'))
            try:
                run_sms = threading.Thread(target=self.send_code, args=(data.get('phone_number'), sms_code))
                run_sms.start()
            except Exception as e:
                print("send phone error", e)
        return sms_code

    def send_code(self, phone, code):
        headers = {'content-type': 'application/json', 'Authorization': 'Bearer '}
        data = {
            "mobile_phone": f"{phone[1:]}",
            "message": f"{code} - Tasdiqlash kodi",
        }
        url = ""
        try:
            content = requests.post(url, headers=headers, data=json.dumps(data))
        except Exception as e:
            print("error send code e=", e)
