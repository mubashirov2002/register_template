from django.contrib import admin
from apps.user.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'fullname', 'birthday', 'is_active', 'phone_number', 'date_joined']
    search_fields = ['date_joined__date', 'id', 'username', 'fullname', 'birthday', 'phone_number']


admin.site.register(User, UserAdmin)
