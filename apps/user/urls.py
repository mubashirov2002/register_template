from django.urls import path
from apps.user import views

app_name = "user"
urlpatterns = [
    path('', views.register, name='register'),
    path('validate/', views.validate_code, name='validate-code'),
    path('succes-page/', views.succes_page, name='succes-page'),

]
