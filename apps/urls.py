from django.urls import path, include
from apps.user import urls as user_urls

urlpatterns = [
    path('', include(user_urls)),
]
